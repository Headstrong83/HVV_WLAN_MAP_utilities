#!/bin/python3
# ./RawData_to_PythonDict.py inputfile "station name"
import sys
import json
import base64

def writetofile(s,name):
    out = open("../HVV_WLAN_MAP_DATA_INTERNAL/"+name+"b64.wifis","w")
    out.write(s)
    out.close()


if not len(sys.argv) == 3:
    print('./RawData_to_PythonDict.py inputfile "station name"')
    sys.exit(0)
filedata = ""
with open(str(sys.argv[1]),"r") as rawfile:
    filedata = rawfile.read()
    rawfile.close()

filedata = filedata.rstrip()
SSID = filedata.split(", BSSID:")[0].replace("SSID: ","")
BSSID = filedata.split(", BSSID:")[1].split(", SEC:")[0]
if ", Streng:" in filedata:
    SEC = filedata.split(", BSSID:")[1].split(", SEC:")[1].split(", Streng:")[0]
    dBm = filedata.split(", Streng:")[1]
    Station = ""
    ArraySSID = SSID.split(", ")
    ArrayBSSID = BSSID.split(",")
    ArraySEC = SEC.split(",")
    ArraydBm = dBm.split(",")
    for a,b,c,d in zip(ArraySSID , ArrayBSSID , ArraySEC, ArraydBm):
        Station += (base64.b64encode(a.encode('utf-8')).decode('utf-8') +";"+b.replace(" ","")+";"+c.replace(" ","")+";"+d.replace(" ","")+"\n")
    print(Station)
    writetofile(Station,str(sys.argv[2]))
else:
    SEC = filedata.split(", SEC:")[1]
    Station = ""
    ArraySSID = SSID.split(", ")
    ArrayBSSID = BSSID.split(",")
    ArraySEC = SEC.split(",")
    for a,b,c in zip(ArraySSID , ArrayBSSID , ArraySEC):
        Station += (base64.b64encode(a.encode('utf-8')).decode('utf-8') +";"+b.replace(" ","")+";"+c.replace(" ","")+"\n")
    print(Station)
    writetofile(Station,str(sys.argv[2]))
